# SMSGear for Samsung Gear S3

This application is only a sample demonstrating that the Samsung Gear S3 is fully capable to view/send SMS from any Android Smartphone (and not only Samsung)

While this application is only a demonstration, there is a lot of potential to make it awesome. If anyone wish to contribute to this project, you're more than welcome.

Be aware, I only had the Samsung Gear S3 for few days, hence why I leave the project as is for the time being.

## What works

* Viewing SMS
* Sending SMS
* Setting conversations limit
* Setting messages limit per conversation

## What is missing
* Make a new conversation
* So many things that could be done
* More error handling

## What can't be done

* Delete SMS [Android Limitation]
* Delete conversations [Android Limitation]

## Known Bugs
1. Let me know

## How to
1. Install the Android App (with Android Studio)
2. Run the Android app for the first time and grant all permissions
3. Install the watch version (with Tizen Studio)
4. Run the Application on the Watch
5. Enjoy!

![](http://i.imgur.com/2ncqQnO.png)

![](http://i.imgur.com/pZBtcZg.png)