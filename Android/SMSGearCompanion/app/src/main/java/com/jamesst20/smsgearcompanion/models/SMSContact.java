package com.jamesst20.smsgearcompanion.models;

import java.util.ArrayList;
import java.util.List;

public class SMSContact {
    private final String name;
    private final int id;
    private final List<String> phones = new ArrayList<>();

    public SMSContact(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public List<String> getPhones() {
        return phones;
    }
}
